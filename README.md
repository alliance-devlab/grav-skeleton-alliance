# Alliance Skeleton

Alliance Skeleton for Grav, built on Quark Open Publishing.


## Installation:

The following `install` script detects if running within Cloudron container (requires a LAMP container and upgrades to PHP8.0 as necessary). Otherwise, inialize the script from within an empty project directory, by running:


##### Note for Private Repos:
**To Install a custom Grav site** built on the Alliance Skeleton, set the `$URL_CUSTOM` environment variable before preceeding to download and run the install script:
```bash
export URL_CUSTOM=https://$USER:$DEPLOY_TOKEN@gitlab.com/$OWNER/path/to/private/repo.git
```

Otherwise, **to install the Alliance Skeleton** simply paste the following into your desired root for the Grav installation:
```bash
wget -q -O - https://gitlab.com/alliance-devlab/grav-skeleton-alliance/-/raw/main/install | bash
```
This will:

 * Extract Grav Core to project root

 * Clone Alliance Skeleton to ./user directory

 * Install all theme and plugin dependencies

### _The Alliance Grav Theme is now installed!_

### Read the Documentation

To learn how to customize your Grav site by reading the documentation.

#### [Alliance Skeleton Documentation](https://gitlab.com/alliance-devlab/grav-skeleton-alliance)

#### [Quark Open Publishing Documentation](https://learn.hibbittsdesign.org/openpublishingspace)

#### [Grav Documentation](https://learn.getgrav.org/16/content/content-pages)

### Local Development:

#### The `dev` script

 * To launch dev server run:
```./dev serve```

 * To build custom scss in ./user/themes/alliance/scss run:
```./dev build```

 * To watch custom scss for changes run:
```./dev watch```

### Private Development (SSH Authentication):

Generate ssh key-pair with the following:
```#? gen ssh example.com``` (ssh-keygen)

Upload public [deploy key](https://docs.gitlab.com/ee/user/project/deploy_keys/index.html#create-a-project-deploy-key) to private Gitlab repository.


<!--
This repository is to be cloned into an existing Grav Installation. Begin by installing Grav CMS, then clone the ```grav-skeleton-alliance``` repo in place of the ```/user``` directory in the root of the Grav installation.


## Local Development Environment

### 1) Install Grav CMS: [Grav Installation Guide](https://learn.getgrav.org/16/basics/installation)

To get started building your Alliance Grav site, first prepare either a development or production environment by following the [Grav Installation Guide](https://learn.getgrav.org/16/basics/installation).


## 2) Download Alliance Skeleton:
```bash
wget -q -O - https://gitlab.com/alliance-devlab/grav-skeleton-alliance/-/raw/main/install | bash
```



## 2) Clone Alliance Skeleton:

After backing up any custom configuration or pages, proceed by replacing the ```/user``` directory with the Alliance Skeleton:

```shell
# backup ./user directory:
mv ./user ./user.backup
# clone alliance skeleton:
git clone https://gitlab.com/alliance-devlab/grav-skeleton-alliance ./user
# install dependencies:
./user/install
```



-->
